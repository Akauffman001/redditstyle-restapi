'use strict'
var express = require("express");
var app = express();
var routes = require("./routes.js");

var jsonParser = require("body-parser").json;
var logger = require("morgan");

//colorful status codes for api res
app.use(logger("dev"));
//return middleware that can be added to app
app.use(jsonParser());

//requests starting with questions r ushered into routes module.
app.use("/questions", routes);

//specify port to serve app on
//port 3000 unless deploying the app to a production environment
var port = process.env.PORT || 3000;
//start server
app.listen(port, function(){
    console.log("Express server is listening on port", port);
});
