'use strict';
var express = require("express");
//set up new router
var router = express.Router();

//GET /questions | return all questions
router.get("/", function(req, res){
 res.json({response: "You sent me a get req"});
           });

//POST /questions | create question
router.post("/", function(req, res){
 res.json({response: "You sent me a post req",
            body: req.body
            });
           });

//GET /questions/:id | return specific questions
router.get("/:qID", function(req, res){
 res.json({response: "You sent me a get req for a specific ID " + req.params.id 
           });
           });

//POST /questions/:id/answers | create an answer
router.post("/:qID/answers", function(req, res){
 res.json({
           response: "You sent me a post req to /answers",
           questionId: req.params.qID,
            body: req.body
            });
           });
//PUT /questions/:qid/answers/:aid | edit answer
router.put("/:qID/answers/:aID", function(req, res){
    res.json({response: "You sent me a put req to /answers",
           questionId: req.params.qID,
            answerID: req.params.aID,
            body: req.body
            });
           });
//DELETE /questions/:qid/answers/:aid | delete specific answer
router.delete("/:qID/answers/:aID", function(req, res){
    res.json({response: "You sent me a delete req to /answers",
           questionId: req.params.qID,
            answerID: req.params.aID
             });
           });

//POST /questions/:qid/answers/:aid/vote-up||vote-down | vote on specific answer
router.post("/:qID/answers/:aID/vote-:dir", function(req, res){
    res.json({response: "You sent me a POST req to /vote-" + req.params.dir,
           questionId: req.params.qID,
            answerID: req.params.aID,
            vote: req.params.dir
             });
           });


//pass router into main app
module.exports = router;
